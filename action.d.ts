export declare enum Action {
    list = 0,
    ditto = 1,
    new = 2,
    save = 3,
    delete = 4,
    none = 5
}
