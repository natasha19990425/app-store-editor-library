export var Action;
(function (Action) {
    Action[Action["list"] = 0] = "list";
    Action[Action["ditto"] = 1] = "ditto";
    Action[Action["new"] = 2] = "new";
    Action[Action["save"] = 3] = "save";
    Action[Action["delete"] = 4] = "delete";
    Action[Action["none"] = 5] = "none";
})(Action || (Action = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWN0aW9uLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYWN0aW9uLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE1BQU0sQ0FBTixJQUFZLE1BT1g7QUFQRCxXQUFZLE1BQU07SUFDaEIsbUNBQUksQ0FBQTtJQUNKLHFDQUFLLENBQUE7SUFDTCxpQ0FBRyxDQUFBO0lBQ0gsbUNBQUksQ0FBQTtJQUNKLHVDQUFNLENBQUE7SUFDTixtQ0FBSSxDQUFBO0FBQ04sQ0FBQyxFQVBXLE1BQU0sS0FBTixNQUFNLFFBT2pCIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGVudW0gQWN0aW9uIHtcbiAgbGlzdCxcbiAgZGl0dG8sXG4gIG5ldyxcbiAgc2F2ZSxcbiAgZGVsZXRlLFxuICBub25lXG59XG4iXX0=