export enum Action {
  list,
  ditto,
  new,
  save,
  delete,
  none
}
