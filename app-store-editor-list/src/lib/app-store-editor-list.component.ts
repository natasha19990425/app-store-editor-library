import { Component, EventEmitter, Input, Output } from '@angular/core';
import { RadioButtonModule } from 'primeng/radiobutton';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { FormsModule } from '@angular/forms';
import { AppStore } from '@his-viewmodel/app-store/dist';
import { AppStoreReq } from '@his-viewmodel/app-store/dist/app/app-store-req';

const PrimeGroup = [RadioButtonModule, TableModule, ButtonModule];
@Component({
  selector: 'his-app-store-editor-list',
  standalone: true,
  imports: [FormsModule, PrimeGroup],
  templateUrl: './app-store-editor-list.component.html',
  styleUrls: ['./app-store-editor-list.component.scss']
})
export class AppStoreEditorListComponent {

  @Input() appStores!: AppStore[];

  @Output() rowItemClick: EventEmitter<AppStore> = new EventEmitter();
  @Output() search: EventEmitter<AppStoreReq> = new EventEmitter();

  appStoreReq: AppStoreReq = new AppStoreReq({});

  /**
   * 表格樣式
   */
  get tableStyle() {

    return { 'min-width': '50rem' };
  }

  /**
   * 按下Enter開始查詢
   * @memberof AppStoreEditorListComponent
   */
  onSearchKeyUp() {

    this.search.emit(this.appStoreReq);
  }

  /**
   * 按下搜尋按鈕
   * @memberof AppStoreEditorListComponent
   */
  onSearchListClick() {

    this.search.emit(this.appStoreReq);
  }

  /**
   * 按下單行資料
   * @memberof AppStoreEditorListComponent
   */
  onRowItemClick(appStore: AppStore) {

    this.rowItemClick.emit(appStore);
  }

}
