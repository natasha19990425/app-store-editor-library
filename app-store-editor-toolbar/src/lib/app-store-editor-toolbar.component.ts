import { Component, Input, WritableSignal } from '@angular/core';
import { NgTemplateOutlet } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { RadioButtonModule } from 'primeng/radiobutton';
import { ToolbarModule } from 'primeng/toolbar';
import { Action } from 'action';

const primeGroup = [ButtonModule, RadioButtonModule, ToolbarModule];
@Component({
  selector: 'his-app-store-editor-toolbar',
  standalone: true,
  imports: [NgTemplateOutlet, primeGroup],
  templateUrl: './app-store-editor-toolbar.component.html',
  styleUrls: ['./app-store-editor-toolbar.component.scss']
})
export class AppStoreEditorToolbarComponent {

  Action = Action;
  @Input() tabViewIndex!: number;
  @Input() action!: WritableSignal<Action>;

  /**
   * toolbar的點擊
   * @param {Action} action 使用者點擊的按鈕
   * @memberof AppStoreEditorToolbarComponent
   */
  onToolbarClick(action: Action) {

    this.action.set(action);
  }

  /**
   * 確認是否在info頁面
   * @memberof AppStoreEditorToolbarComponent
   */
  isDetail() {

    return this.tabViewIndex === 1;
  }



}

