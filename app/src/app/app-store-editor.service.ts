import { Injectable, inject } from '@angular/core';
import { AppStore, AppStoreService } from '@his-viewmodel/app-store/dist'
import { AppStoreReq } from '@his-viewmodel/app-store/dist/app/app-store-req'
import { HttpClient } from '@angular/common/http';
import { lastValueFrom } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AppStoreEditorService implements AppStoreService {

  #httpClient: HttpClient = inject(HttpClient);
  #url = "http://localhost:3000/webapi/app-store-editor";

  /**
   * 新增應用程式
   * @param {AppStore} 需新增的應用程式
   * @return { Promise<boolean>} 新增結果
   * @memberof AppStoreEditorService
  */
  insertAppStore(appstore: AppStore): Promise<boolean> {

    const result$ = this.#httpClient.post<boolean>(this.#url, appstore)
    return lastValueFrom(result$);
  }

  /**
   * 取得應用程式
   * @param {number} appId 需查詢的應用程式
   * @return { Promise<AppStore>} 查詢結果
   * @memberof AppStoreEditorService
   */
  getAppStore(appId: number): Promise<AppStore> {

    const result$ = this.#httpClient.get<AppStore>(`${this.#url}/${appId}`);
    return lastValueFrom(result$);
  }

  /**
   * 取得應用程式清單
   * @param {AppStoreReq} 搜尋條件
   * @return {Promise<AppStore[]>} 依搜尋條件查詢應用程式清單結果
   * @memberof AppStoreEditorService
   */
  getAppStores(appStoreReq: AppStoreReq): Promise<AppStore[]> {

    const result$ = this.#httpClient.get<AppStore[]>(`${this.#url}/${appStoreReq.searchType}/${appStoreReq.searchValue}`);
    return lastValueFrom(result$);
  }

  /**
   * 更新應用程式
   * @param {AppStore} 需更新的應用程式
   * @return { Promise<boolean>} 更新結果
   * @memberof AppStoreEditorService
   */
  updateAppStore(appstore: AppStore): Promise<boolean> {

    const result$ = this.#httpClient.patch<boolean>(this.#url, appstore);
    return lastValueFrom(result$);
  }

  /**
   * 刪除應用程式
   * @param {number}appId 需刪除的應用程式
   * @return { Promise<boolean>}刪除結果
   * @memberof AppStoreEditorService
   */
  deleteAppStore(appId: number): Promise<boolean> {

    const result$ = this.#httpClient.delete<boolean>(`${this.#url}/${appId}`);
    return lastValueFrom(result$);
  }


}


