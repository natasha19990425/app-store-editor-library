import { Component, effect, inject, signal } from '@angular/core';
import { AppStoreEditorToolbarComponent } from 'app-store-editor-toolbar/src/public-api';
import { TabViewModule } from 'primeng/tabview';
import { AppStoreEditorListComponent } from 'app-store-editor-list/src/public-api';
import { AppStoreEditorInfoComponent } from 'app-store-editor-info/src/public-api';
import { AppStore } from '@his-viewmodel/app-store';
import { AppStoreEditorService } from './app-store-editor.service';
import { DividerModule } from 'primeng/divider';
import { DialogModule } from 'primeng/dialog';
import { AppStoreReq } from '@his-viewmodel/app-store';
import { ButtonModule } from 'primeng/button';
import { Action } from 'action';
import { NgTemplateOutlet } from '@angular/common';
const primeGroup = [TabViewModule, DividerModule, DialogModule, ButtonModule]

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'app-root',
  standalone: true,
  imports: [AppStoreEditorListComponent, AppStoreEditorInfoComponent, AppStoreEditorToolbarComponent, primeGroup,NgTemplateOutlet],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  appStores: AppStore[] = [];
  appStore: AppStore = new AppStore({ appId: 0, icon: '/assets/image/image1.png' });
  action = signal(Action.none);

  tabViewIndex = 1;
  dialogVisible = false;

  #appStoreEditorService: AppStoreEditorService = inject(AppStoreEditorService);

  actionHandler: Record<number, () => void> = {

    [Action.list]: () => this.handleItemListClick(),
    [Action.new]: () => this.handleNewItemClick(),
    [Action.ditto]: () => this.handleDittoItemClick(),
    [Action.save]: () => this.handleSaveItemClick(),
    [Action.delete]: () => this.handleDeleteClick(),
    [Action.none]: () => 0, //do nothing
  };

  actionChange = effect(() => {

    this.actionHandler[this.action()]();
  })

  /**
   * 彈窗樣式
   * @memberof AppComponent
   */
  get dialogStyle() {

    return { width: '25vw', height: '20vh' };
  }

  /**
   * 切換頁籤
   * @param {number} tabViewIndex 需要切換的頁籤
   * @memberof AppComponent
   */
  changeTabViewIndex(tabViewIndex: number) {

    this.tabViewIndex = tabViewIndex;
  }

  /**
   * 監聽app-store-editor-list的點擊事件
   * @memberof AppComponent
   */
  async onRowItemClick(appStore: AppStore) {

    this.changeTabViewIndex(1);

    try {
      const currentAppStore = await this.#appStoreEditorService.getAppStore(appStore.appId);
      this.appStore = currentAppStore;

    } catch (error) {
      console.error(error);
    }
  }


  /**
   * 按下確認刪除鈕
   * @memberof AppComponent
   */
  onOkClick() {

    this.dialogVisible = false;
    try {
      this.#appStoreEditorService.deleteAppStore(this.appStore.appId);
    } catch (error) {
      console.error(error);
    }
  }

  /**
   * 按下取消刪除鈕
   * @memberof AppComponent
   */
  onCancelClick() {

    this.dialogVisible = false;
  }

  /**
   * 按下關閉彈窗按鈕
   * @memberof AppComponent
   */
  onHide() {

    this.dialogVisible = false;
  }


  /**
   * 監聽搜尋事件
   * ＠param {AppStoreReq} appsStoreReq 搜尋條件
   * @memberof AppComponent
   */
  async onSearch(appsStoreReq: AppStoreReq) {

    try {
      this.appStores = await this.#appStoreEditorService.getAppStores(appsStoreReq);
    } catch (error) {
      console.error(error);
    }
  }

  /**
   * 監聽toolbar的清單鈕點擊事件
   * @memberof AppComponent
   */
  handleItemListClick() {

    this.changeTabViewIndex(0);
  }

  /**
   * 監聽toolbar的新增鈕點擊事件
   * @memberof AppComponent
   */
  handleNewItemClick() {

    this.changeTabViewIndex(1);
    this.appStore = new AppStore({ appId: 0, icon: '/assets/image/image1.png' });
  }

  /**
   * 監聽toolbar的Ditto鈕點擊事件
   */
  handleDittoItemClick() {

    this.appStore = Object.assign(this.appStore, { appId: 0 });
  }

  /**
   * 監聽toolbar的儲存鈕點擊事件
   */
  handleSaveItemClick() {

    try {
      if (this.appStore.appId === 0) {
        this.#appStoreEditorService.insertAppStore(this.appStore);
      } else {
        this.#appStoreEditorService.updateAppStore(this.appStore);
      }
    } catch (error) {
      console.error(error);
    }
  }

  /**
   * 監聽toolbar的刪除鈕點擊事件
   * @memberof AppComponent
   */
  handleDeleteClick() {

    this.dialogVisible = true;
  }

}
