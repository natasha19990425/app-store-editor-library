const { shareAll, withModuleFederationPlugin } = require('@angular-architects/module-federation/webpack');

module.exports = withModuleFederationPlugin({

  name: 'Appcomponent',

  exposes: {
    './Appcomponent': './app/src/app/app.routes.ts',
  },

  shared: {

    ...shareAll({ singleton: true, strictVersion: false, requiredVersion: 'auto' }),
  },

});
